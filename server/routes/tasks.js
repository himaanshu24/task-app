const express = require('express');
var router = express.Router();
const Tasks = require('../models/Tasks');

router.post('/', function (req, res, next) {
	const taskList = new Tasks(req.body);

	taskList.save()
		.then(() => {
			res.json('Task addedd successfully');
		})
		.catch( err => {
			res.status(400).json('Error while creating the product');
		})
});

// Get Task List
router.get('/:pid', function(req, res) {
	const pid = req.params.pid;
	Tasks.find({'pid': pid}, function(err, product) {
		res.json(product)
	})
});

router.get('/', function(req, res) {
	Tasks.find().then( tasklist => {
		res.json(tasklist);
	})
})

router.put('/', function (req, res) {
	const tid = req.query.taskId;
	const status = req.query.status;
	Tasks.update({_id: tid}, {$set: {status: status}},
	(err) => {
		if (err) res.status(500).json(err);
		res.json('Task updated successfully!');
	});
});

module.exports = router;