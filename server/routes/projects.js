const express = require('express');
var router = express.Router();
const Projects = require('../models/Projects');

router.post('/', function (req, res, next) {
	const projectList = new Projects(req.body);

	projectList.save()
		.then(() => {
			res.json('Project addedd successfully');
		})
		.catch(err => {
			res.status(400).json('Error while creating the product');
		})
});

// Get Project List
router.get('/:pid', function (req, res) {
	const pid = req.params.pid;
	Projects.find({
		'pid': pid
	}, function (err, product) {
		res.json(product)
	})
});

router.get('/', function (req, res) {
	Projects.find().then(projectList => {
		res.json(projectList);
	})
})

router.put('/:prjId', function (req, res) {
	const tid = req.params.prjId;
	Projects.findOneAndUpdate({
		'prjId': tid
	}, req.body, function (err) {
		if (err) res.status(500).json(err);
		res.json('Project updated successfully!');
	});
});

module.exports = router;