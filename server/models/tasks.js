const mongoose = require('mongoose');

var Schema = mongoose.Schema;

var Tasks = new Schema({
	taskId: {type: Number},
	heading: {type: String},
	project: {type: String},
	desc: {type: String},
	status: {type: String}
}, {collection: 'tasks'});

module.exports = mongoose.model('Tasks', Tasks);