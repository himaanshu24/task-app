const mongoose = require('mongoose');

var Schema = mongoose.Schema;

var Projects = new Schema({
	prjId: {type: Number},
	prjName: {type: String},
	type: {type: String},
	desc: {type: String},
	desc: {type: String}
}, {collection: 'projects'});

module.exports = mongoose.model('Projects', Projects);