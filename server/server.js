const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const tasks = require('./routes/tasks');
const projects = require('./routes/projects');
const cors = require('cors');

const app = express();


const DB = 'mongodb://localhost:27017/taskApp';
mongoose.connect(DB, {userNewUrlParser: true})
	.then( () => {
			console.log('Database is connected');
		},
		(err) => {
			console.log('Error connecting to database', err);
		}
	);

app.use(bodyParser.urlencoded({
	extend: true
}));

app.use(bodyParser.json());
app.use(cors());

app.use('/', (req, res, next) => {
	console.log('Current Date is:', Date.now());
	next();
});

app.get('/', (req, res) => {
	res.send('Hello World');
});

const PORT = 3200;
app.use('/tasks', tasks);
app.use('/projects', projects);

app.listen(PORT, () => {
	console.log('Running ');
})