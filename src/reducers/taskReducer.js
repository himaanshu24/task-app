import {
	ADD_TASK,
	UN_STARTED,
	IN_PROGRESS,
	COMPLETED,
	CHANGE_TASK_STATUS,
	DISPLAY_TASK,
	LOAD_TASK_LIST
} from '../actions/index';

let count = new Date();
const cardDetail = [];

const taskDetail = (state = cardDetail, action) => {
	switch(action.type) {
		case LOAD_TASK_LIST:
			return [...state, ...action.taskList || []]
		case DISPLAY_TASK:
			return action.taskList;
		case CHANGE_TASK_STATUS:
			return [...state.map(a => {
				if (a.taskId === action.taskId) {
					return { ...a, status: action.status}
				} else {
					return a;
				}
			})]
		default:
			return state;
	}
}

export default taskDetail;