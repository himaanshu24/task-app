import { combineReducers } from 'redux';
import cardDetail from './taskReducer';
import projectDetail from './projectReducer';

const taskApp = combineReducers({
	cardDetail,
	projectDetail
});

export default taskApp;