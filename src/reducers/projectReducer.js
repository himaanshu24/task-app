import {
	DISPLAY_PROJECT,
	LOAD_PROJECT_LIST
} from '../actions/index';

const prjoectDetail = [];
let prId = 0;

const projecDetail = (state = prjoectDetail, action) => {
	switch (action.type) {
		case LOAD_PROJECT_LIST:
			return [...state, ...action.projectList || []]
		case DISPLAY_PROJECT:
			return action.projectList;
		default:
			return state;
	}
}

export default projecDetail;