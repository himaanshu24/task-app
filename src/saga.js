import { takeEvery, all, fork, put, call } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import {
	ADD_TASK,
	ADD_PROJECT,
	LOAD_TASK_LIST,
	LOAD_PROJECT_LIST,
	DISPLAY_TASK,
	DISPLAY_PROJECT,
	CHANGE_TASK_STATUS
} from '../src/actions/index';
import axios from 'axios';
import CONFIG from './config';


function* listTasks() {
	const response = yield call(axios.get, 'http://localhost:3200/tasks');
	yield put({type: LOAD_TASK_LIST, taskList: response.data});
}

function* listProject() {
	const response = yield call(axios.get, 'http://localhost:3200/projects');
	yield put({type: LOAD_PROJECT_LIST, projectList: response.data})
}

function* storeTask(action) {
	var formObj = {};
	for (const inp of action.formObj) {
		if (inp.name) {
			formObj = { ...formObj,
				[inp.name]: inp.value
			}
		}
	}
	yield call(axios.post, 'http://localhost:3200/tasks', formObj);
	const response = yield call(axios.get, 'http://localhost:3200/tasks');
	yield put({type: DISPLAY_TASK, taskList: response.data});
}

function* saveTask() {
	yield takeEvery(ADD_TASK, storeTask)
}

function* storeProject(action) {
	var formObj = {};
	for (const inp of action.formObj) {
		if (inp.name) {
			formObj = { ...formObj,
				[inp.name]: inp.value
			}
		}
	}
	yield call(axios.post, 'http://localhost:3200/projects', formObj);
	const response = yield call(axios.get, 'http://localhost:3200/projects');
	yield put({
		type: DISPLAY_PROJECT,
		projectList: response.data
	});
}

function* saveProject() {
	yield takeEvery(ADD_PROJECT, storeProject)
}

function* updateTask(action) {
	yield call(axios.put, `http://localhost:3200/tasks?taskId=${action.taskId}&status=${action.status}`);
	const response = yield call(axios.get, 'http://localhost:3200/tasks');
	yield put({type: DISPLAY_TASK, taskList: response.data});
}

function* changetask() {
	yield takeEvery(CHANGE_TASK_STATUS, updateTask)
}

// Root Saga
export default function* rootSaga() {
	yield all([
		fork(listTasks),
		fork(listProject),
		fork(saveTask),
		fork(saveProject),
		fork(changetask)
	]);
}
