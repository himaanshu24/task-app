const CONFIG = {
	'DB_URL': 'mongodb://localhost:27017/tasksapp',
	'SERVER_PORT': 4200,
	'SERVER_URL': 'http://localhost:4200',
	'ENDPOINT_USERS': '/users',
	'ENDPOINT_PROJECTS': '/projects',
	'ENDPOINT_TASKS': '/tasks',
	'ENDPOINT_LOGIN': '/login',

}

export default CONFIG;
