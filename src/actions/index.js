// Actions

export const ADD_PROJECT = 'ADD_PROJECT';
export const ADD_TASK = 'ADD_TASK';
export const UN_STARTED = 'UN_STARTED';
export const IN_PROGRESS = 'IN_PROGRESS';
export const COMPLETED = 'COMPLETED';
export const CHANGE_TASK_STATUS = 'CHANGE_TASK_STATUS';
export const LIST_PROJECT = 'LIST_PROJECT';
export const LIST_TASK_IN_PROJECT = 'LIST_TASK_IN_PROJECT';
export const LOAD_TASK_LIST = 'LOAD_TASK_LIST';
export const LOAD_PROJECT_LIST = 'LOAD_PROJECT_LIST';
export const DISPLAY_TASK = 'DISPLAY_TASK';
export const DISPLAY_PROJECT = 'DISPLAY_PROJECT';



// Actions Creator

export const addProject = (formObj) => ({
	type: ADD_PROJECT,
	formObj
});

export const addTask = (formObj) => ({
	type: ADD_TASK,
	formObj
})

export const changeTaskStatus = (taskId, status) => ({
	type: CHANGE_TASK_STATUS,
	taskId, status
})

export const listTasksInProject = projectId => ({
	type: LIST_TASK_IN_PROJECT,
	projectId
})

export const loadTaskList = () => ({
	type: LOAD_TASK_LIST
})

export const displayTask = () => ({
	type: DISPLAY_TASK
})

export const displayProject = () => ({
	type: DISPLAY_PROJECT
})

export const loadProjectList = () => ({
	type: LOAD_PROJECT_LIST
})

export const loadtasks = (data) => ({
	type: 'LOAD_TASKS',
	data
})