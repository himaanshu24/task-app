import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import Router from './components/Router';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Provider } from 'react-redux';
import store from './store'

ReactDOM.render(<Provider store={store}>
	<Router />
</Provider>, document.getElementById('root'));
registerServiceWorker();
