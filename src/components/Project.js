import React from 'react';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import Sidebar from './Sidebar';
import NewProject from './NewProject';
import ProjectList from './ProjectList';
import ProjectDetail from './ProjectDetail';

const sidebarLists = [
	{name: 'New Project', path: '/newproject'},
	{name: 'List Project', path: '/projectlist'}
];

const Project = () => (
	<div className="container-fluid" style={{ marginTop: '68px' }}>
		<div className="row">
			<Sidebar sidebarLists={sidebarLists} />
			<div className="main-content col-md-8">
				<Switch>
					<Route exact path='/newproject' component={NewProject} />
					<Route exact path='/projectlist' component={ProjectList} />
					<Route exact path='/projectdetail' component={ProjectDetail} />
				</Switch>
			</div>
		</div>
	</div>
)

export default Project;