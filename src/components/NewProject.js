import React from 'react';
import { connect } from 'react-redux';
import { addProject } from '../actions/index';

const NewProject = ({dispatch}) => (
	<div className="container">
		<div className="row justify-content-md-center">
			<div className="col-md-8 order-md-1">
				<form className="needs-validation" noValidate="" onSubmit={(evt) => {
					evt.preventDefault();
					dispatch(addProject(evt.target));
				}}>
					<div className="mb-3">
						<label htmlFor="firstName">Project name</label>
						<input name="prjName" type="text" className="form-control" id="firstName" placeholder="Please enter Project name" required="" />
						<div className="invalid-feedback">
							Project name is required.
						</div>
					</div>
					<div className="mb-3">
						<label htmlFor="lastName">Project Desc</label>
						<input name="desc" type="text" className="form-control" id="lastName" placeholder="Please enter Project Desc" required="" />
						<div className="invalid-feedback">
							Project Description is required.
						</div>
					</div>
					<div className="mb-3">
						<label htmlFor="lastName">Project Type</label>
						<input name="type" type="text" className="form-control" id="lastName" placeholder="Please enter Project Type" required="" />
						<div className="invalid-feedback">
							Project Type is required.
						</div>
					</div>
					<button className="btn btn-primary btn-lg btn-block" type="submit">Save</button>
				</form>
			</div>
		</div>
	</div>
)


export default connect()(NewProject)