import React from 'react';
import { connect } from 'react-redux';
import { listTasksInProject } from '../actions/index';

const ProjectDetail = ({tasksInProject}) => (
	<div className="container">
		<div className="row justify-content-md-center">
			<div className="col-md-8 order-md-1">
				<table className="table table-striped">
					<thead>
						<tr>
							<th scope="col">Name</th>
							<th scope="col">Status</th>
						</tr>
					</thead>
					<tbody>
						{
							tasksInProject.map((item, index) => (
								<tr key={index} id={item.prjId} >
									<td>{item.heading}</td>
									<td>{item.status}</td>
								</tr>
							))
						}
					</tbody>
				</table>
			</div>
		</div>
	</div>
)

const mapStateToProps = (state) => ({
	tasksInProject: state.cardDetail
})

export default connect(
	mapStateToProps
)(ProjectDetail)