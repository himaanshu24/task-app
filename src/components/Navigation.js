import React from 'react';
import { Link } from 'react-router-dom';

const Navigation = ({navigationList}) => (
	<nav className="navbar fixed-top flex-md-nowrap p-0 shadow">
		<a className="navbar-brand col-sm-3 col-md-2 mr-0" href="/">Task Application</a>
		<ul className=" inline">
			{
				navigationList.map((item, index) => (
					<li key={index} className="list-inline-item">
						<Link className="nav-link" to={item.path}>{item.name}</Link>
					</li>
				))
			}
		</ul>
	</nav>
)

export default Navigation;