import React from 'react';
import Card from './Card';
import { connect } from 'react-redux';
import {
	UN_STARTED,
	IN_PROGRESS,
	COMPLETED,
	LOAD_TASK_LIST,
	loadTaskList,
	changeTaskStatus
} from '../actions/index';

const TaskList = ({cardDetail, onChangeFn}) => (
	<div className="container">
		<div className="row justify-content-md-center">
			<div className="col-md-4">
				<h5>Unstarted</h5>
				{
					cardDetail.map((item, index) => item.status === UN_STARTED ?
							<Card key={index} cardDetail={item} onChangeFn={onChangeFn}/> : false)
				}
			</div>
			<div className="col-md-4">
				<h5>In Progress</h5>
				{
					cardDetail.map((item, index) => item.status === IN_PROGRESS ?
							<Card key={index} cardDetail={item} onChangeFn={onChangeFn}/> : false )
				}
			</div>
			<div className="col-md-4">
				<h5>Completed</h5>
				{
					cardDetail.map((item, index) => item.status === COMPLETED ?
							<Card key={index} cardDetail={item} onChangeFn={onChangeFn}/> : false )
				}
			</div>
		</div>
	</div>
)

class Task extends React.Component {
	constructor(props) {
		super(props);
	}
	componentDidMount() {
		this.props.onLoadFn();
	}
	render() {
		return <TaskList {...this.props} />
	}
}

const mapStateToProps = (state) => ({
	cardDetail: state.cardDetail
})

const mapDispatchToProps = (dispatch) => ({
	onChangeFn: (taskId, status) => dispatch(changeTaskStatus(taskId, status)),
	onLoadFn: () => dispatch(loadTaskList())
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Task)