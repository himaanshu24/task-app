import React from 'react';
import {
	UN_STARTED,
	IN_PROGRESS,
	COMPLETED
} from '../actions/index';

const selectBox = React.createRef();

const backgroundClass = (cardDetail) => {
	if (cardDetail.status === IN_PROGRESS) {
		return 'bg-warning'
	} else if (cardDetail.status === COMPLETED) {
		return 'bg-success'
	} else {
		return 'bg-danger'
	}
};

const Card = ({ cardDetail, onChangeFn }) => (
	<div className={`card ${backgroundClass(cardDetail)} mb-3`} style={{ maxWidth: '18rem' }}>
		<div className='card-header'>{cardDetail.heading}</div>
		<div className='card-body'>
			<h5 className='card-title'>{cardDetail.project}</h5>
			<p className='card-text'>{cardDetail.desc}</p>
			<select id={cardDetail._id} ref={selectBox} value={cardDetail.status} className='custom-select d-block w-100' onChange={(evt) => {
					onChangeFn(evt.currentTarget.getAttribute('id'), evt.target.value)
				}}>
				<option value={UN_STARTED}>Unstarted</option>
				<option value={IN_PROGRESS}>In Progress</option>
				<option value={COMPLETED}>Completed</option>
			</select>
		</div>
	</div>
);

export default Card;