import React from 'react';
import { connect } from 'react-redux';
import {
	addTask,
	UN_STARTED,
	IN_PROGRESS,
	COMPLETED
} from '../actions/index'

const NewTask = ({cardDetail, onSubmitFn}) => (
	<div className="container">
		<div className="row justify-content-md-center">
			<div className="col-md-8 order-md-1">
				<form className="needs-validation" noValidate="" onSubmit={(evt) => {
					evt.preventDefault();
					onSubmitFn(evt.target);
				}}>
					<div className="form-group row">
						<label className="col-sm-3 col-form-label" htmlFor="firstName">Name</label>
						<div className="col-sm-9">
							<input type="text" name="heading" className="form-control" id="firstName" placeholder="Task Name" required="required" />
							<div className="invalid-feedback">
								Task name is required.
							</div>
						</div>
					</div>
					<div className="form-group row">
						<label className="col-sm-3" htmlFor="lastName">Description</label>
						<div className="col-sm-9">
							<input type="text" name="desc" className="form-control" id="lastName" placeholder="Task Description" required="" />
							<div className="invalid-feedback">
								Project Description is required.
							</div>
						</div>
					</div>
					<div className="form-group row">
						<label className="col-sm-3" htmlFor="lastName">Project</label>
						<div className="col-sm-9">
							<select name="project" className="custom-select d-block w-100" id="project-type" required="">
								<option value="">Choose...</option>
								{
									cardDetail.map((a, i) => (<option key={i}>{a.project}</option>))
								}
							</select>
							<div className="invalid-feedback">
								Project is required.
							</div>
						</div>
					</div>
					<div className="form-group row">
						<label className="col-sm-3" htmlFor="lastName">Status</label>
						<div className="col-sm-9">
							<select name="status" className="custom-select d-block w-100" id="task-status">
								<option value="">Choose...</option>
								<option value={UN_STARTED}>{UN_STARTED}</option>
								<option value={IN_PROGRESS}>{IN_PROGRESS}</option>
								<option value={COMPLETED}>{COMPLETED}</option>
							</select>
						</div>
					</div>
					<button className="btn btn-primary btn-lg btn-block" type="submit">Save</button>
				</form>
			</div>
		</div>
	</div>
)

const mapStateToProps = (state) => ({
	cardDetail: state.cardDetail
})

const mapDispatchToProps = (dispatch) => ({
	onSubmitFn: (formObj) => dispatch(addTask(formObj))
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(NewTask)