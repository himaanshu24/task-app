import React from 'react';
import {BrowserRouter, Route, Switch, Link} from 'react-router-dom';
import MainContent from './MainContent';

const Router = () => (
	<BrowserRouter>
		<div className="main-app">
			<MainContent />
		</div>
	</BrowserRouter>
);

export default Router;