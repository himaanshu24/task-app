import React from 'react';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import Sidebar from './Sidebar';
import NewTask from './NewTask';
import TaskList from './TaskList';

const sidebarLists = [
	{ name: 'New Task', path: '/newtask' },
	{ name: 'List Task', path: '/tasklist' }
];

const Task = () => (
	<div className="container-fluid" style={{ marginTop: '68px' }}>
		<div className="row">
			<Sidebar sidebarLists={sidebarLists} />
			<div className="main-content col-md-8">
				<Switch>
					<Route exact path='/newtask' component={NewTask} />
					<Route exact path='/tasklist' component={TaskList} />
				</Switch>
			</div>
		</div>
	</div>
)

export default Task;