import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { listTasksInProject, loadProjectList } from '../actions/index';

const ProjectList = ({projectDetail, onClickFn}) => (
	<div className="container">
		<div className="row justify-content-md-center">
			<div className="col-md-8 order-md-1">
				<table className="table table-striped">
					<thead>
						<tr>
							<th scope="col">Name</th>
							<th scope="col">Desc</th>
							<th scope="col">Type</th>
						</tr>
					</thead>
					<tbody>
						{
							projectDetail.map((item, index) => (
								<tr key={index} id={item.prjId} onClick={(evt) => onClickFn(evt.target.id)}>
									<td><Link to="/projectdetail">{item.prjName}</Link></td>
									<td>{item.type}</td>
									<td>{item.desc}</td>
								</tr>
							))
						}
					</tbody>
				</table>
			</div>
		</div>
	</div>
)

class Project extends React.Component {
	constructor(props) {
		super(props);
	}
	componentDidMount() {
		this.props.onLoadFn();
	}
	render() {
		return <ProjectList {...this.props} />
	}
}

const mapStateToProps = (state) => ({
	projectDetail: state.projectDetail
})

const mapDispatchToProps = (dispatch) => ({
	onClickFn: projectId => dispatch(listTasksInProject(projectId)),
	onLoadFn: () => dispatch(loadProjectList())
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Project)