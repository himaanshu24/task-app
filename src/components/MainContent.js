import React from 'react';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import Navigation from './Navigation';
import Project from './Project';
import Task from './Task';

const navigationList = [
	{name: 'Projects', path: '/newproject'},
	{name: 'Task', path: '/newtask'},
	{name: 'SignOut', path: '/signpage'},
];

const MainContent = () => (
	<div>
		<Navigation navigationList={navigationList} / >
		<Switch>
			<Route path='/*project*' component={Project} />
			<Route path='/*task*' component={Task} />
		</Switch>
	</div>
)

export default MainContent;